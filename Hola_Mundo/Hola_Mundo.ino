/* Hola Mundo Microprosesadores
	
	Hacer que parpadear un led
*/

void setup (){       
        pinMode(11, OUTPUT);
        pinMode(8, OUTPUT);	
}

void loop (){
        int led = 11;
        digitalWrite(led, HIGH);
	delay(100);
	digitalWrite(led, LOW);
	delay(50);
	digitalWrite(led, HIGH);
	delay(100);
	digitalWrite(led, LOW);
	delay(50);
        led = 8;
        digitalWrite(led, HIGH);
	delay(1000);
	digitalWrite(led, LOW);
	delay(50);
	digitalWrite(led, HIGH);
	delay(100);
	digitalWrite(led, LOW);
	delay(50);
	
}
