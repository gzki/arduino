
const int ledRojo = 3;    // LED RGB indicando los pines
const int ledVerde = 5;
const int ledAzul = 6;
const int potRojo = 0;    // Pines de los potenciometros
const int potVerde = 1;
const int potAzul = 2;
int rojo, verde, azul;   // Guardar valores de los potenciometros                              

void setup() {
  Serial.begin (9600);        // Inicia la comunicacion Serial
  pinMode(ledRojo, OUTPUT);    // Funcionamiento de los pines de los leds
  pinMode(ledVerde, OUTPUT);
  pinMode(ledAzul, OUTPUT);
  
  // pines analogicos de los potenciometros son declaradors
  // por default como entrada
}

void loop() {
  rojo = analogRead(potRojo) / 4;   // se guarda el valor de los 
  verde = analogRead(potVerde) / 4;  // potenciometros y se divide el valor
  azul = analogRead(potAzul) / 4;  // entre 4 para usarlo despues
  
  analogWrite(ledRojo, rojo);   // se usa el valor guardado para
  analogWrite(ledVerde, verde);  // iluminar el led indicado
  analogWrite(ledAzul, azul);
  
  Serial.print("Rojo: ");
  Serial.print(rojo);
  Serial.print(", ");
  
  Serial.print("Verde: ");
  Serial.print(verde);
  Serial.print(", ");
  
  Serial.print("Azul: ");
  Serial.println(azul);
  
  delay(500);
  
}
