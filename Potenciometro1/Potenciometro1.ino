
const int led = 3;     //  LED  analogo (PWN)
int brillo;            // Variable oara controlar el brillo
const int pot = 0;     // Potenciometro

void setup() {
  pinMode(led, OUTPUT);
  // PINS analogico son declarados como entrada por default
}

void loop() {
  brillo = analogRead (pot) / 4;  // El valor del potenciometro se divide entre 4 para que sea compateible con AnalgWrite
  analogWrite (led, brillo);
}
